# micuento

Micuento test api

This project uses PostgreSQL as DBMS, you need to copy *.env.dev* to *.env* and fill the configuration inside.

To run it, execute the following commands:

```
npm install
npm run dev
```

To run the unit tests, execute the following commands (You need to connect the PostgreSQL DB first to be able to run the tests):

```
npm run test
```

