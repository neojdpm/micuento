import { createConnection } from 'typeorm';
import { databaseConfig } from './database/database';
import TaskController from './routes/task/task.routes';
import "reflect-metadata";
import { Request, Response } from 'express';
import { handleError } from './middlewares/errorHandler';

var express = require('express');
var app = express();

const PORT = process.env.PORT || 3000;

createConnection(databaseConfig as any).then(connection => {
    app.use(express.urlencoded({ extended: false }));
    app.use(express.json());
    app.use('/', new TaskController(connection).router);

    // 404 Handler 
    app.use( (req: Request, res: Response) => {
      res.status(404);
      res.type('txt').send('Not found');
    });

    // General error handler
    app.use(handleError);

    app.listen(PORT, function() {
        console.log(`Micuento API started in port ${PORT}!`);
    });
}).catch(error => console.log(error));
