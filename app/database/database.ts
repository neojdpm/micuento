require('dotenv').config();

export const databaseConfig = {
    type: 'postgres',
    username: process.env.DB_USER,
    host: process.env.DB_HOST || '127.0.0.1',
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    port: process.env.POSTGRES_PORT || 5432,
    entities: [__dirname + '/../**/*.entity.{js,ts}'],
    synchronize: true,
    logging: false
}