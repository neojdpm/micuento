import {Entity, Column} from "typeorm";
import { BaseModel } from '../base.model';

@Entity()
export class Task extends BaseModel {

    @Column()
    name!: string;

    @Column()
    priority!: number;

    @Column()
    dueDate!: Date;

}