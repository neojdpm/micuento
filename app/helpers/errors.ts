export class NotFoundError {
    public status = 404;
    public message: string;

    constructor ( message: string ) {
        this.message = message;
    }
}

export class ValidationError {
    public status = 400;
    public errors: any;

    constructor ( errors: any ) {
        this.errors = errors;
    }
}