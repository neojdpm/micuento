
import { Request, Response, NextFunction } from 'express';
import { NotFoundError, ValidationError } from '../helpers/errors';

/**
 * Basic error handling for validation and generic errors passed through next function
 */
export function handleError(err: any, req: Request, res: Response, next: NextFunction) {
    console.error(err);
    res.status(err.status);
    switch (err.constructor) {
        case ValidationError: 
            return res.send(err.errors);
        case NotFoundError:
            return res.send(err.message);
        default:
            console.log('Error not defined');
            res.send(500);
    }
};