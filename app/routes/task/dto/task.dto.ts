import { IsDate, IsDateString, IsIn, IsInt, IsNotEmpty, IsNumber, IsOptional, IsString, Max, Min } from 'class-validator';

export class CreateTask {
    @IsString()
    @IsNotEmpty()
    name: string;

    @IsInt()
    @Min(1)
    @Max(5)
    @IsNotEmpty()
    priority: number;
    
    @IsDateString()
    @IsNotEmpty()
    dueDate?: string;

    constructor( { name, priority, dueDate }: CreateTask ) {
        this.name = name;
        this.priority = priority;
        this.dueDate = dueDate;
    }

}

export class UpdateTask {

    @IsNumber()
    @IsNotEmpty()
    id: number;
    
    @IsString()
    @IsOptional()
    name?: string;

    @IsInt()
    @Min(1)
    @Max(5)
    @IsOptional()
    priority?: number;
    
    @IsDateString()
    @IsOptional()
    dueDate?: string;

    constructor( { id, name, priority, dueDate }: UpdateTask ) {
        this.name = name;
        this.priority = priority;
        this.dueDate = dueDate;
        this.id = id;
    }
}

export class GetTask {
    @IsString()
    @IsOptional()
    sort?: string;

    @IsIn(['overdue', 'pending'])
    @IsOptional()
    type?: string;
    
    constructor( { sort, type }: GetTask ) {
        this.sort = sort;
        this.type = type;
    }
}