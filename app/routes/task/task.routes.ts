import { validate } from 'class-validator';
import { Router, Request, Response, NextFunction } from 'express';
import { ValidationError } from '../../helpers/errors';
import { CreateTask, UpdateTask, GetTask } from './dto/task.dto';
import TaskService from './task.service';

export default class TaskController {
    public path = '/task';
    public getPath = '/task/:id';
    public router = Router();
    private taskService: TaskService;

    constructor ( connection: any ) {
        this.taskService = new TaskService(connection);
        this.router.put(this.path + '/update', this.putTask)
        this.router.post(this.path + '/create', this.postTask)
        this.router.delete(this.getPath, this.deleteTask)
        this.router.get(this.path, this.getTasks)
    }

    getTasks = async (request: Request, response: Response, next: NextFunction) => {
        try {
            const data = new GetTask(request.query);
            await this.validateInput(data);
            const tasks = await this.taskService.getTasks(data);
            response.send(tasks);
        } catch (error) {
            next(error);
        }
    }

    deleteTask = async (request: Request, response: Response, next: NextFunction) => {
        try {
            const task = await this.taskService.deleteTask( Number(request.params.id) );
            response.send(task);
        } catch (error) {
            next(error);
        }
    }

    postTask = async (request: Request, response: Response, next: NextFunction) => {
        try {
            const data = new CreateTask(request.body);
            await this.validateInput(data);
            const task = await this.taskService.postTask(data);
            response.send(task);
        } catch (error) {
            next(error);
        }
    }

    putTask = async (request: Request, response: Response, next: NextFunction) => {
        try {
            const data = new UpdateTask(request.body);
            await this.validateInput(data);
            const task = await this.taskService.putTask(data);
            response.send(task);
        } catch (error) {
            next(error);
        }
    }

    validateInput = async (validationClass: any) => {
        return new Promise( (resolve, reject) => {
            validate(validationClass).then(errors => {
                // errors is an array of validation errors
                if (errors.length > 0) {
                    reject( new ValidationError(errors));
                } else {
                    console.log('validation succeed');
                    resolve(true);
                }
            });
        })
       
    }

}