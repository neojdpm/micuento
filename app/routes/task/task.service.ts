import { LessThanOrEqual, MoreThan } from 'typeorm';
import { Task } from '../../database/entities/task.entity';
import { NotFoundError } from '../../helpers/errors';
import { CreateTask, UpdateTask, GetTask } from './dto/task.dto';

const MoreThanDate = () => MoreThan(new Date())
const LessThanDate = () => LessThanOrEqual(new Date())

/**
 * Contains the main funcionality for the Task CRUD
 */
export default class TaskService {
    private taskRepository;

    constructor ( connection: any ) {
        this.taskRepository = connection.getRepository(Task) 
    }

    async getTasks (data: GetTask) {
        let tasks = [];

        // Separating pending an overdue in two selects
        if (!data.type || data.type === 'pending') {
            const pendingTasks = await this.taskRepository.find({ 
                dueDate: MoreThanDate()
            });
            tasks.push(...pendingTasks);
        }
        if (!data.type || data.type === 'overdue') {
            const overDueTasks = await this.taskRepository.find({ 
                dueDate: LessThanDate()
            });
            overDueTasks.map((x: any) => x.overdue = true );
            tasks.push(...overDueTasks);
        }

        // Sorting tasks 

        if (data.sort) {
            tasks = tasks.sort(this.parseSort(data.sort));
        }

        return tasks;
    }

    async deleteTask(id: number) {
        const taskFound = await this.taskRepository.findOne({ id });
        if (taskFound) {
            const task = await this.taskRepository.remove(taskFound);
            return task;
        } else throw new NotFoundError('Task not found');
    }

    async postTask(data: CreateTask) {
        const task = await this.taskRepository.save(data);
        return task;
    }

    async putTask(data: UpdateTask) {
        const taskFound = await this.taskRepository.findOne({ id: data.id });
        if (taskFound) {
            const task = await this.taskRepository.save(data);
            return task;
        } else throw new NotFoundError('Task not found');
    }

    // returns a sorting function
    private parseSort( sort: string) {
        const desc = sort[0] === '-';
        const property = desc ? sort.slice(1) : sort;
        return (a: any, b: any) => {
            return desc ? b[property] - a[property] : a[property] - b[property];
        };
    }

}