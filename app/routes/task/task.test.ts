import { Connection, createConnection } from 'typeorm';
import { databaseConfig } from '../../database/database';
import { NotFoundError } from '../../helpers/errors';
import TaskService from './task.service';

const TEST_DB_NAME = 'micuento_test';

let connection: Connection;
let taskService: TaskService;

describe('Tasks', () => {
    beforeAll(async () => {
        // The master connection creates a custom db for testing without mocking.
        console.log('Creating connection');
        const masterConnection = await createConnection(databaseConfig as any);
        await masterConnection.query(`DROP DATABASE IF EXISTS ${TEST_DB_NAME}`);
        await masterConnection.query(`CREATE DATABASE ${TEST_DB_NAME}`);
        await masterConnection.close();

        const testDbConfig = { ...databaseConfig }
        testDbConfig.database = TEST_DB_NAME;
        connection = await createConnection(testDbConfig as any);
        taskService = new TaskService(connection);
    });

    afterAll(async () => {
        console.log('Closing connection');
        await connection.close();
    });

    it('Should resolve with no tasks', async () => {
        const tasks = await taskService.getTasks({});
        expect(tasks.length).toBe(0);
        console.log(tasks);
    })

    it('Should create a new task', async () => {
        const task = await taskService.postTask({
            name: "Overdue",
            dueDate: "2020-09-12",
            priority: 1,
        });
        expect(task.name).toBe('Overdue');
    })

    it('Should update the created task', async () => {
        const task = await taskService.putTask({
            name: "OverdueUpdated",
            id: 1,
        });
        expect(task.name).toBe('OverdueUpdated');
    })
    
    it('Should create a new pending task', async () => {
        const task = await taskService.postTask({
            name: "Pending",
            dueDate: "2050-09-12",
            priority: 5,
        });
        expect(task.name).toBe('Pending');
    })
    
    it('Should resolve with two tasks', async () => {
        const tasks = await taskService.getTasks({});
        expect(tasks.length).toBe(2);
    })
    
    it('Should resolve with one overdue task', async () => {
        const tasks = await taskService.getTasks({ type: 'overdue'});
        expect(tasks.length).toBe(1);
        expect(tasks[0].overdue).toBe(true);
    })
    
    it('Should resolve with one pending task', async () => {
        const tasks = await taskService.getTasks({ type: 'pending'});
        expect(tasks.length).toBe(1);
    })
    
    it('Should delete one task', async () => {
        await taskService.deleteTask(1);
        const tasks = await taskService.getTasks({});
        expect(tasks.length).toBe(1);
    })
    
    it('Cant delete an invalid task', async () => {
        expect(taskService.deleteTask(10)).rejects.toEqual(new NotFoundError('Task not found'))
    })
})